FROM --platform=$TARGETPLATFORM offlineinternet/olip-base:latest as build

WORKDIR /opt/

RUN apt update && \
    apt install -y make g++ go-bindata

RUN ARCH="$(dpkg --print-architecture | awk -F- '{ print $NF }')" && \
	case $ARCH in \
		armhf) node_build='https://nodejs.org/dist/v16.8.0/node-v16.8.0-linux-armv7l.tar.gz' ;; \
		amd64) node_build='https://nodejs.org/dist/v16.8.0/node-v16.8.0-linux-x64.tar.gz' ;; \
		i386) node_build='https://unofficial-builds.nodejs.org/download/release/v16.4.2/node-v16.4.2-linux-x86.tar.gz' ;; \
		arm64) node_build='https://nodejs.org/dist/v16.8.0/node-v16.8.0-linux-arm64.tar.gz' ;; \
		*) echo >&2 "error: unsupported architecture: $ARCH"; exit 1 ;; \
	esac && \
	wget --quiet $node_build -O node.tar.gz && \
	tar -C /usr/local -xzf node.tar.gz --strip=1

RUN npm install -g less less-plugin-clean-css

RUN ARCH="$(dpkg --print-architecture | awk -F- '{ print $NF }')" && \
	case $ARCH in \
		armhf) arch='armv6l' ;; \
		amd64) arch='amd64' ;; \
		i386) arch='386' ;; \
		arm64) arch='arm64' ;; \
		*) echo >&2 "error: unsupported architecture: $ARCH"; exit 1 ;; \
	esac && \
	wget --quiet "https://golang.org/dl/go1.16.5.linux-$arch.tar.gz" && \
	tar -C /usr/local -xzf go1.16.5.linux-$arch.tar.gz && \
    ln -s /usr/local/go/bin/go /usr/local/bin/go

RUN mkdir -p /go/src/github.com/writefreely/writefreely
WORKDIR /go/src/github.com/writefreely/writefreely

RUN git clone https://github.com/writefreely/writefreely.git /go/src/github.com/writefreely/writefreely && \
	git checkout main

ENV GO111MODULE=on

RUN make build

RUN make ui

RUN mkdir /stage && \
    cp -R /usr/local/go/bin/go \
      /go/src/github.com/writefreely/writefreely/templates \
      /go/src/github.com/writefreely/writefreely/static \
      /go/src/github.com/writefreely/writefreely/pages \
      /go/src/github.com/writefreely/writefreely/cmd \
      /stage

# Final image
FROM --platform=$TARGETPLATFORM offlineinternet/olip-base:latest

COPY --from=build /stage /go

WORKDIR /go

ADD supervisor/* /etc/supervisor/conf.d/
RUN ln -sf /run/supervisord.log /var/log/supervisor/supervisord.log

COPY entrypoint.sh /

COPY config.ini .

EXPOSE 80

ENTRYPOINT ["/entrypoint.sh"]

CMD ["/usr/bin/supervisord"]