#!/bin/sh

DOMAIN_NAME="http://writefreely.$(echo $APPLICATION_ROOT | cut -d ':' -f1)"

sed -i '/host                  =/c\host                  = '$DOMAIN_NAME config.ini

cd /go
/go/cmd/writefreely/writefreely keys generate
/go/cmd/writefreely/writefreely db init

exec "$@"
